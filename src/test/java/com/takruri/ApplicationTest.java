package com.takruri;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.delete;
import static io.restassured.module.mockmvc.RestAssuredMockMvc.get;
import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.WebApplicationContext;

import io.restassured.module.mockmvc.RestAssuredMockMvc;

/**
 * This is the main test class for the Messages Microservice
 *
 */
@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
class ApplicationTest {
	
	@Autowired
	private WebApplicationContext webApplicationContext;

	@BeforeEach
	public void initialiseRestAssuredMockMvcWebApplicationContext() {
	    RestAssuredMockMvc.webAppContextSetup(webApplicationContext);
	}
	
	/**
	 * test adding 2 palindrome messages 
	 */
	@Test
	@Order(1)
	public void addMessagePalindromeMessage() {
		
		given().
			contentType("application/json").
			body("{\n" + "  \"content\": \"abcba\"\n" + "}").
		when().
			post("/messages/").
		then().
			statusCode(HttpStatus.OK.value()).
		and().
			body("palindrome", equalTo(Boolean.TRUE));
		
		given().
			contentType("application/json").
			body("{\n" + "  \"content\": \"123321\"\n" + "}").
		when().
			post("/messages/").
		then().
			statusCode(HttpStatus.OK.value()).
		and().
			body("palindrome", equalTo(Boolean.TRUE));
		
	}
	
	/**
	 * test adding 2 non-palindrome messages 
	 */
	@Test
	@Order(2)
	public void addMessageNonPalindromeMessage() {
		
		given().
			contentType("application/json").
			body("{\n" + "  \"content\": \"abcde\"\n" + "}").
		when().
			post("/messages/").
		then().
			statusCode(HttpStatus.OK.value()).
		and().
			body("palindrome", equalTo(Boolean.FALSE));
		
		given().
			contentType("application/json").
			body("{\n" + "  \"content\": \"123456\"\n" + "}").
		when().
			post("/messages/").
		then().
			statusCode(HttpStatus.OK.value()).
		and().
			body("palindrome", equalTo(Boolean.FALSE));
	}
	
	/**
	 * test getting a message by id
	 */
	@Test
	@Order(3)
	public void getMessageById() {
		
		get("/messages/1").
		then().
			statusCode(HttpStatus.OK.value()).
		and().
			body("content", equalTo("abcba")).
		and().
			body("palindrome", equalTo(Boolean.TRUE));
	}
	
	/**
	 * test getting a message by a non valid id
	 */
	@Test
	@Order(4)
	public void getMessageByIdNotFound() {
		
		get("/messages/999").
		then().
			statusCode(HttpStatus.NOT_FOUND.value()).
		and().
			body("message", equalTo("message.not.found"));
	}
	
	/**
	 * test deleting a message by id
	 */
	@Test
	@Order(5)
	public void deleteMessage() {
		
		delete("/messages/1").
		then().
			statusCode(HttpStatus.OK.value());
	}
	
	/**
	 * test editing a message by id
	 */
	@Test
	@Order(6)
	public void editMessage() {
		
		given().
			contentType("application/json").
			body("{\n" + "  \"content\": \"787\"\n" + "}").
		when().
			put("/messages/2").
		then().
			statusCode(HttpStatus.OK.value()).
		and().
			body("content", equalTo("787")).
		and().
			body("palindrome", equalTo(Boolean.TRUE));
	}
	
	/**
	 * test listing all messages
	 */
	@Test
	@Order(7)
	public void listAll() {
		
		get("/messages/").
		then().
			statusCode(HttpStatus.OK.value()).
		and().
			body("content", hasItems("787", "abcde", "123456"));
	}
}

