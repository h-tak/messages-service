package com.takruri.controllers.beans;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Bean class for the message request
 */
public class NewMessage {
	
	@NotNull(message = "must.not.be.empty")
	@Size(min = 1, max = 128, message = "should.be.between.1.128")
	private String content;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
