package com.takruri.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.takruri.controllers.beans.NewMessage;
import com.takruri.domain.Message;
import com.takruri.services.MessageService;

@RestController
@RequestMapping(path = "/messages")
public class MessagesController {

	@Autowired
	private MessageService messageService;

	/*
	 * Endpoint to return all messages
	 */
	@GetMapping("/")
	List<Message> findAll() {
		return messageService.findAll();
	}

	/*
	 * Endpoint to return message by id
	 */
	@GetMapping("/{messageId}")
	public Message getMessage(@PathVariable Integer messageId) {
		return messageService.getMessageById(messageId);
	}

	/*
	 * Endpoint to delete message by id
	 */
	@DeleteMapping("/{messageId}")
	void deleteEmployee(@PathVariable Integer messageId) {
		messageService.deleteMessageById(messageId);
	}

	/*
	 * Endpoint to create a new message
	 */
	@PostMapping("/")
	Message createMessage(@RequestBody @Valid NewMessage newMessage) {
		return messageService.saveNewMessage(newMessage);
	}
	
	/*
	 * Endpoint to update message by id
	 */
	@PutMapping("/{messageId}")
	Message updateMessage(@PathVariable Integer messageId, @RequestBody @Valid NewMessage newMessage) {
		return messageService.updateMessage(messageId, newMessage);
	}
}
