package com.takruri.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.takruri.controllers.beans.NewMessage;
import com.takruri.domain.Message;
import com.takruri.exception.MessageException;
import com.takruri.repositories.MessageRepository;

/**
 *	The main message service class, It interacts with MessageRepository and PalindromeService
 */
@Service
public class MessageService {
	
	@Autowired
	private MessageRepository messageRepository;
	
	@Autowired
	private PalindromeService palindromeService;

	/*
	 * adds a new message after finding out if it is palindrome
	 */
	public Message saveNewMessage(NewMessage newMessage) {
		
		Message message = new Message();
		message.setContent(newMessage.getContent());
		message.setPalindrome(palindromeService.isPalindrome(newMessage.getContent()));
		
		return messageRepository.save(message);
	}

	/*
	 * gets a message by id
	 */
	public Message getMessageById(Integer messageId) {
		return messageRepository.findById(messageId)
				.orElseThrow(() -> new MessageException("message.not.found", HttpStatus.NOT_FOUND));
	}

	/*
	 * deletes message by id
	 */
	public void deleteMessageById(Integer messageId) {
		if (messageRepository.existsById(messageId)) {
			messageRepository.deleteById(messageId);
		}
	}

	/*
	 * updates an existing message and find out if it is palindrome
	 */
	public Message updateMessage(Integer messageId, NewMessage newMessage) {
		
		return messageRepository.findById(messageId).map(message -> {
			message.setContent(newMessage.getContent());
			message.setPalindrome(palindromeService.isPalindrome(newMessage.getContent()));
			return messageRepository.save(message);
		})
		.orElseThrow(() -> new MessageException("message.not.found", HttpStatus.NOT_FOUND));
	}
	
	/*
	 * retrieve all messages
	 */
	public List<Message> findAll() {
		return messageRepository.findAll();
	}
}
