package com.takruri.services;

import org.springframework.stereotype.Service;

/**
* This service calculates if a given message is palindrome or not
*/
@Service
public class PalindromeService {
	
	/**
	* The main method in this service, it will calculate if a given message is palindrome or not
	*/
	public boolean isPalindrome(String message) {
		
		for (int i=0; i<message.length()/2; i++) {
			if (message.charAt(i) != message.charAt(message.length()-1-i)) {
				return false;
			}
		}
		
		return true;
	}
}
