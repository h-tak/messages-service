package com.takruri.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.takruri.domain.Message;

/**
 * JPA repository for the messages
 */
public interface MessageRepository extends JpaRepository<Message, Integer> {

}
