package com.takruri.exception;

import org.springframework.http.HttpStatus;

/**
 * Runtime exception for the Message service
 */
public class MessageException extends RuntimeException {
	
	private static final long serialVersionUID = -6126012323623798204L;
	private HttpStatus httpStatus;
	
	public MessageException() {
		super();
	}

	public MessageException(String message, HttpStatus httpStatus) {
		super(message);
		this.httpStatus = httpStatus;
	}

	public MessageException(String message, Throwable cause) {
		super(message, cause);
	}

	public MessageException(Throwable cause) {
		super(cause);
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}
}

