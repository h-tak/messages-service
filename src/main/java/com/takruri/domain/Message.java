package com.takruri.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Bean class for the message JPA entity
 */
@Entity
public class Message {

	private @Id @GeneratedValue Integer id;
	private String content;
	private Boolean isPalindrome;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Boolean isPalindrome() {
		return isPalindrome;
	}

	public void setPalindrome(Boolean isPalindrome) {
		this.isPalindrome = isPalindrome;
	}
}
