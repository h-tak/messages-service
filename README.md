# Messages Microservice

This microservice is responsible for managing and processing client messages. It allows clients to create, update, and retrieve messages. In addition, this microservice will compute if the messages are palindrome or not.

## Build, Deploy, and Run

Messages Microservice is coded using Java and uses a set of Java libraries including Spring Boot to run a web server, Swagger to provide REST API documentation, and H2 in-memory JPA compliant database.

To build the service, you will need maven and JDK v1.8 or later.
To build the project use this command:
```
mvn clean package spring-boot:repackage
```
this command will create an executable JAR file under the `target` directory.
To Run the JAR file, execute the following command:
```
java -jar target/messages-service-0.0.1-SNAPSHOT.jar
```
Spring Boot web server will start listening on port `8080`
You can access the Swagger UI and try the endpoints using:
```
http://localhost:8080/swagger-ui.html
```

## Architecture 

![Architecture](https://bitbucket.org/h-tak/messages-service/raw/e31b79d0744734fc76cb08341a6f09cc8a9bfa68/diagram.png)

The microservice is divided into three main components:

 - Controller Tier
 - Service Tier
 - Repository Tier
 
### Controller Tier

The controller tier has one controller `MessagesController`. It is responsible for receiving the client request, validating it, passing it the service tier to be processed, and finally generate the response. This controller defines a set of Restful endpoints as follows:
 - `GET /messages/`: this endpoint will return all stored messages in the system
 - `GET /messages/{messageId}`: this endpoint will return a single message given its `messageId`
 - `POST /messages`: clients call this endpoint to create a new message and store it in the system
 - `PUT /messages/{messageId}`: clients call this endpoint to update an existing message in the system given its `messageId`
 - `DELETE /messages/{messageId}`: clients call this endpoint to delete an existing message in the system given its `messageId`

### Service Tier

The service tier has two services:

- `MessageService` this service handles the request from the controller and pass it to the  Repository tier after processing the message request. i.e. finding out if it is palindrome or not.
- `PalindromeService` this service will calculate if the message is palindrome or not.

### Repository Tier

The repository tier has one repository `MessageRepository` that store and retrieve messages from H2 database using JPA.

### System business entities

 - `NewMessage` defines the JSON object the client sends to create or update a message. It has one field `content` that holds the message text.
 - `Message` defines the JPA entity that is stored in the system. it has:
 -- `id` a unique auto incremented integer.
 -- `content` the message text.
 -- `isPalindrome` a  true/false flag to tell is the message is palindrome or not.


